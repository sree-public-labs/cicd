# CICD Quick Reference and Snippets

# 1. Smallest
This job will in the test stage which is the default
```yaml
job1:
    script:
        - echo "ABCD"
```
# 3. Basic Starter Pipeline
```yaml
stages: # List of stages for jobs, and their order of execution
- build
- test
- deploy

build-job: # This job runs in the build stage, which runs first.
stage: build
script:
- echo "Compiling the code..."

unit-test-job: # This job runs in the test stage.
stage: test # It only starts when the jobs in the build stage completes successfully.
script:
- echo "Running unit tests... This will take about 60 seconds."

lint-test-job: # This job also runs in the test stage.
stage: test # It can run at the same time as unit-test-job (in parallel).
script:
- echo "Linting code..."

deploy-job: # This job runs in the deploy stage.
stage: deploy # It only runs when *both* jobs in the test stage complete successfully.
script:
- echo "Deploying application..."
```
# 4. Only Condition
This runs only when the pipeline runs in the main branch
Create a new branch called stage (choose create from main)
![image.png](./image.png)
```yaml
main-build:
  stage: build
  script:
    - echo Building Main
  only:
    - main
branch-build:
  stage: build
  script:
    - echo Building Main
  only:
    - stage
```
# 5. Environment
![image-1.png](./image-1.png)
```yaml
stages:
  - stage
staging:
  stage: stage
  script:
  - echo "Yes Staged"
  environment:
    name: staging
    url: https://sree.cc/
  # only:
  # - stage
```
# 6. Variables

## 7. If
This example uses the default test stage to keep it simple. Here we are using just the if condidition. 
```yaml
variables:
  MY_FLAG: "true"
my-test:
  script:
  - if [ "$MY_FLAG" == "true" ]; then MODULE="echo YES"; else MODULE="echo NO"; fi
  - ${MODULE}
```
Multi-line if
```yaml
variables:
  MY_FLAG: "true"
my-test:
  script:
   - >
    if [ "$MY_FLAG" == "true" ]; then
      echo YES
    else
      echo NO
    fi
```
![image-9.png](./image-9.png)

# 8. if and when - GitLab Rules
The below job will not run
```yaml
variables:
  MY_FLAG: "true"
workflow:
  rules:
    - if: '$MY_FLAG == "true"'
      when: never
    - if: '$MY_FLAG == "yes"'
      when: never
    - when: always
my-test:
  script:
   - echo "I AM RUNNING"
```
Below job will run
```yaml
variables:
  MY_FLAG: "true"
workflow:
  rules:
    - if: '$MY_FLAG == "true"'
      when: always
    - if: '$MY_FLAG == "yes"'
      when: always
    - when: never
my-test:
  script:
   - echo "I AM RUNNING"
```
![image-10.png](./image-10.png)
# 9. Hiding a Job
adding a fullstop in front of the job name hides it
```yaml
stages:
  - abcd
abcd-stage:
  stage: abcd
  script:
    echo "Hello from Job ABCD-STAGE"
.xyz-stage:
  stage: abcd
  script:
    echo "Hello from Job XYZ-STAGE"
```
# 10. Extends
![image-2.png](./image-2.png)
```yaml
stages:
  - abcd
abcd-stage:
  stage: abcd
  script:
    echo "Hello from Job ABCD-STAGE"
.xyz-stage:
  stage: abcd
  script:
    echo "Hello from Job XYZ-STAGE"
pqr-stage:
  extends: .xyz-stage
```
# 11. Anchor
![image-3.png](./image-3.png)
![image-4.png](./image-4.png)
```yaml
stages:
  - abcd
abcd-stage:
  stage: abcd
  script:
    echo "Hello from Job ABCD-STAGE"
.xyz-stage: &xyz  # Creates an anchor named xyz using the hidden job
  stage: abcd
  script:
    echo "Hello from Job XYZ-STAGE"
pqr-stage:
  <<: *xyz
```
# 12. Needs
## With Needs
```yaml
linux-build:
  stage: build
  script:
    - sleep 5
mac-build:
  stage: build
  script:
    - sleep 30
linux-test:
  stage: test
  needs:
    - linux-build
  script:
    - sleep 5
mac-test:
  stage: test
  needs:
    - mac-build
  script:
    - sleep 30
mac-deploy:
  stage: deploy
  script:
    - sleep 30
  needs:
    - mac-test
linux-deploy:
  stage: deploy
  needs:
    - linux-test
  script:
    - sleep 5
```
![image-83.png](./image-83.png)
![image-84.png](./image-84.png)
![image-85.png](./image-85.png)
![image-86.png](./image-86.png)
![image-87.png](./image-87.png)
![image-88.png](./image-88.png)
![image-89.png](./image-89.png)
![image-90.png](./image-90.png)
![image-91.png](./image-91.png)
![image-92.png](./image-92.png)
![image-93.png](./image-93.png)
![image-94.png](./image-94.png)
## No Needs
```yaml
linux-build:
  stage: build
  script:
    - sleep 5
mac-build:
  stage: build
  script:
    - sleep 30
linux-test:
  stage: test
  # needs:
  #   - linux-build
  script:
    - sleep 5
mac-test:
  stage: test
  # needs:
  #   - mac-build
  script:
    - sleep 30
mac-deploy:
  stage: deploy
  script:
    - sleep 30
  # needs:
  #   - mac-test
linux-deploy:
  stage: deploy
  # needs:
  #   - linux-test
  script:
    - sleep 5
```
![image-95.png](./image-95.png)
![image-96.png](./image-96.png)
![image-97.png](./image-97.png)
![image-98.png](./image-98.png)
![image-99.png](./image-99.png)
![image-100.png](./image-100.png)
![image-101.png](./image-101.png)
![image-102.png](./image-102.png)
![image-103.png](./image-103.png)
![image-104.png](./image-104.png)


# 13. Dependency and Artifacts
## No dependency, no artifacts - nothing gets downloaded(copied) to the job's work area from the previous stage(jobs)
```yaml
stages:
  - apple
  - orange
job1:
  stage: apple
  script:
    - echo apple-job1 >> file.txt
job2:
  stage: apple
  script:
    - echo apple-job2 >> file.txt
job3:
  stage: apple
  script:
    - echo apple-job3 >> file.txt
job4:
  stage: orange
  script:
    - echo orange-job4 >> file.txt
    - cat file.txt
```
![image-5.png](./image-5.png)
## Artifacts, but no dependency
```yaml
stages:
  - apple
  - orange
job1:
  stage: apple
  script:
    - echo apple-job1 >> file1.txt
  artifacts:
    paths:
      - file1.txt
job2:
  stage: apple
  script:
    - echo apple-job2 >> file2.txt
  artifacts:
    paths:
      - file2.txt
job3:
  stage: apple
  script:
    - echo apple-job3 >> file3.txt
  artifacts:
    paths:
      - file3.txt
job4:
  stage: orange
  script:
    - echo orange-job4 >> file4.txt
    - ls -l *.txt
```
![image-6.png](./image-6.png)
## Artifacts and Dependency
```yaml
stages:
  - apple
  - orange
job1:
  stage: apple
  script:
    - echo apple-job1 >> file1.txt
  artifacts:
    paths:
      - file1.txt
job2:
  stage: apple
  script:
    - echo apple-job2 >> file2.txt
  artifacts:
    paths:
      - file2.txt
job3:
  stage: apple
  script:
    - echo apple-job3 >> file3.txt
  artifacts:
    paths:
      - file3.txt
job4:
  stage: orange
  script:
    - echo orange-job4 >> file4.txt
    - ls -l *.txt
  dependencies:
    - job1
    - job2
```
![image-7.png](./image-7.png)
# 14. Docker Image Build and Push to Container Repo
```yaml
stages:
  - publish
publish:
  image: docker:latest
  stage: publish
  services:
    - docker:dind
  script:
    - docker build -t $TAG_COMMIT .   # MUST HAVE A VALIDE Dockerfile
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
```
Create a file named Dockerfile
```Dockerfile
FROM nginx:alpine

RUN echo "Hello from Sree" > /usr/share/nginx/html/index.html
```
![image-8.png](./image-8.png)

# 15. Include and Trigger
## Create a child.yml if your repo
```yaml
child-build:
  stage: build
  script:
    - echo "Child build"
child-deploy:
  stage: deploy
  script:
    - echo "Child deploy"
```
## Triggering the above child into the main pipeline
```yaml
main-build:
  stage: build
  script:
    - echo "Main Build"
job-child:
  trigger:
    include: child.yml
```
![image-109.png](./image-109.png)
![image-110.png](./image-110.png)
![image-111.png](./image-111.png)
![image-112.png](./image-112.png)
![image-113.png](./image-113.png)

# 16. Cache
```yaml
stages:
  - clone
  - build
cloning:
  stage: clone
  image: schogini/ubuntu-trusty-git-curl-nano-wget
  script:
    - git clone https://github.com/schogini/samplejava.git
    - ls -l
  artifacts:
    paths:
      - samplejava
building:
  stage: build
  image: maven:3.6.3-jdk-8-openj9
  script:
    - cd samplejava
    - mvn clean package -B
    - ls -l target
```
![image-114.png](./image-114.png)
## With Cache
```yaml
stages:
  - clone
  - build1
  - build2
variables:
  MAVEN_OPTS: "-Djava.awt.headless=true -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"

cache:
  paths:
    - .m2/repository
  # keep cache across branch
  key: "$CI_BUILD_REF_NAME"

cloning:
  stage: clone
  image: schogini/ubuntu-trusty-git-curl-nano-wget
  script:
    - git clone https://github.com/schogini/samplejava.git
  artifacts:
    paths:
      - samplejava
build1:
  stage: build1
  image: maven:3.6.3-jdk-8-openj9
  script:
    - cd samplejava
    - mvn clean package -B $MAVEN_OPTS $MAVEN_CLI_OPTS 

build2:
  stage: build2
  image: maven:3.6.3-jdk-8-openj9
  script:
    - cd samplejava
    - mvn clean package -B $MAVEN_OPTS $MAVEN_CLI_OPTS 
```
![image-128.png](./image-128.png)
![image-125.png](./image-125.png)
![image-124.png](./image-124.png)
![image-126.png](./image-126.png)
## Without Cache
```yaml
stages:
  - clone
  - build1
  - build2
variables:
  MAVEN_OPTS: "-Djava.awt.headless=true -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"

# cache:
#   paths:
#     - .m2/repository
#   # keep cache across branch
#   key: "$CI_BUILD_REF_NAME"

cloning:
  stage: clone
  image: schogini/ubuntu-trusty-git-curl-nano-wget
  script:
    - git clone https://github.com/schogini/samplejava.git
  artifacts:
    paths:
      - samplejava
build1:
  stage: build1
  image: maven:3.6.3-jdk-8-openj9
  script:
    - cd samplejava
    - mvn clean package -B $MAVEN_OPTS $MAVEN_CLI_OPTS 
build2:
  stage: build2
  image: maven:3.6.3-jdk-8-openj9
  script:
    - cd samplejava
    - mvn clean package -B $MAVEN_OPTS $MAVEN_CLI_OPTS 
```
![image-127.png](./image-127.png)


# 17. ADDITIONAL WAY TO CREATE CREATE RUNNERS IN THE CLOUD
## Step-1: Create an account at https://hub.docker.com/ (Verify email)
## Step-2: Login at https://labs.play-with-docker.com/
## Step-3: Click Start
![image-11.png](./image-11.png)
## Step-4: Click Add New Instance
![image-12.png](./image-12.png)
## Step-4: Get The Specific Runner Credentials via CICD Settings->Runners Menu
![F0FEFC3D-E72A-44B1-BC98-EE404C2E3979.png](./F0FEFC3D-E72A-44B1-BC98-EE404C2E3979.png)
## Step-5: Start the Runner Container
```
docker run -d -v $PWD:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock --name gitlab-runner gitlab/gitlab-runner:latest
```
## Step-6: Register the Runner
```
docker exec -ti gitlab-runner gitlab-runner register
```
![4DF41B6A-AEC6-4FE4-B4FE-7C57B0234156.png](./4DF41B6A-AEC6-4FE4-B4FE-7C57B0234156.png)

![8713C5B7-1997-4F6D-8EE8-3FC03DBAE445.png](./8713C5B7-1997-4F6D-8EE8-3FC03DBAE445.png)

![image-13.png](./image-13.png)
![image-14.png](./image-14.png)
![image-15.png](./image-15.png)
![image-16.png](./image-16.png)
![image-17.png](./image-17.png)
```yaml
stages:
  - publish
publish:
  image: docker:latest
  stage: publish
  services:
    - docker:dind
  script:
    - docker build -t $TAG_COMMIT .   # MUST HAVE A VALIDE Dockerfile
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
  tags:
    - pwd-r1
```
![image-18.png](./image-18.png)
![image-19.png](./image-19.png)
## Edit config.toml
```
vi config.toml
```
![image-20.png](./image-20.png)
![image-21.png](./image-21.png)
![image-22.png](./image-22.png)
```
docker exec -ti gitlab-runner gitlab-runner restart
```
![image-23.png](./image-23.png)
## Your Specific Runner is Ready - Retry the Failed Job
![image-24.png](./image-24.png)
![image-25.png](./image-25.png)

